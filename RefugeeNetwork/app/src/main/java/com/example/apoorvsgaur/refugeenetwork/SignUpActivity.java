package com.example.apoorvsgaur.refugeenetwork;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);



        Button iamACompanyButton = (Button) findViewById(R.id.iamACompany);
        iamACompanyButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });


        Button iamALocalButton = (Button) findViewById(R.id.iamALocal);
        iamALocalButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });


        Button iamARefugeeButton = (Button) findViewById(R.id.iamARefugee);
        iamARefugeeButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(SignUpActivity.this, Refugee_net.class);
                startActivity(myIntent);
            }
        });





    }
}
